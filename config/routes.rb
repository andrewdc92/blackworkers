Rails.application.routes.draw do
  namespace :api, defaults: { format: :json } do
    resources :users, only: [:new, :create, :show, :edit, :update]
    resource :session, only: [:new, :create, :destroy]
    resource :artists, only: [:new, :create, :destroy, :index]
    
  end
end
