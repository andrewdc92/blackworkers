class ArtistsController < ApplicationController

  def create

  end

  def index
    @artists = Artist.all
  end

  def show
    @artist = Artist.find(params[:id])
  end


private

def artist_params
  params.require(:artist).permit(:name, :city, :artist_id)
end

end
